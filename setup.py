from setuptools import setup, find_packages

import glob
import  os

data_files = []
def recGlob(data_files,source):
	directories = glob.glob(source,recursive=True)
	for directory in directories:
		files = glob.glob(directory+'\*')
		files2 = []

		for f in files:
			if os.path.isdir(f):
				recGlob(data_files,f )
			else: files2.append(f)
		data_files.append((directory, files2))
recGlob(data_files, 'gitlab_test')

setup(name='gitlab_test', version='1.0', packages=find_packages(),package_data={'': ['*.*']}, include_package_data=True,data_files=data_files)